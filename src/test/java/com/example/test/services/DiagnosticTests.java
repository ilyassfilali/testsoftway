package com.example.test.services;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DiagnosticTests {

	@Autowired
	private Diagnostic diagnostic;
	
	@Test
	void Should_Return_Empty_List_Of_Medical_Units() {
		List<String> medicalUnits = this.diagnostic.getPathologie(22);
		assertThat(medicalUnits).isEmpty();
	}
	
	@Test
	void Should_Not_Return_Empty_List_Of_Medical_Units() {
		List<String> medicalUnits = this.diagnostic.getPathologie(33);
		assertThat(medicalUnits).isNotEmpty();
	}
	
	@Test
	void Should_Return_List_Of_Medical_Units_Contains_Cardiologie() {
		List<String> medicalUnits = this.diagnostic.getPathologie(33);
		assertThat(medicalUnits).contains("Cardiologie");
	}

	@Test
	void Should_Return_List_Of_Medical_Units_Contains_Traumatologie() {
		List<String> medicalUnits = this.diagnostic.getPathologie(55);
		assertThat(medicalUnits).contains("Traumatologie");
	}
	
	@Test
	void Should_Return_List_Of_Medical_Units_Contains_Both_Cardiologie_Traumatologie() {
		List<String> medicalUnits = this.diagnostic.getPathologie(15);
		assertThat(medicalUnits.size()).isEqualTo(2);
		assertThat(medicalUnits).contains("Cardiologie");
		assertThat(medicalUnits).contains("Traumatologie");
	}

}
