package com.example.test.services;

import java.util.List;

public interface Diagnostic {
	List<String> getPathologie(int index_santé);
	
}
