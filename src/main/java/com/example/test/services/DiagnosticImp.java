package com.example.test.services;

import java.util.List;
import java.util.Vector;

import org.springframework.stereotype.Service;

@Service
public class DiagnosticImp implements Diagnostic{

	@Override
	public List<String> getPathologie(int index_de_sante) {
		List<String> medicalUnits=new Vector<String>();
		if(index_de_sante%3 == 0) {
			medicalUnits.add("Cardiologie");
		}
		if(index_de_sante%5 == 0) {
			medicalUnits.add("Traumatologie");
		}
		return medicalUnits;
	}

	
}
